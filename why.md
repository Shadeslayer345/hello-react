# ¿Por Que?

TLDR; I wanted to learn some new technologies, outside of a large project. This is essentially a playground/starter kit

Now let's break everything down!

## ES6
First off if you've never seen es6 or know the significance of those two letters when placed nex to the number six mean then [you'd]() [better]() [get]() [caught]() up, but I'll go ahead and break a few basics down:

``` javascript
// 1. constants are a thing now, use them as much as possible!
const x = 0;

// 2. string templates denoted by the back-tick `` let us interpolate variables more easily into string, use everywhere possible
const msg = `VALUE OF X`;
const msgX = `${msg} ${x}!!` // VALUE OF X 0!!

// 3. arrow functions are syntatic sugar that saves time but also binds 'this' to the upper scope
const fun = () => {
    return `This is cool`;
};

// 4. destructuring allows us to take only what we want from an object passed as a function parameter
const obj = {
  name: Destructuring,
  coolness: `Infinitely`,
  ...
};
const destrct = ({name}) => { // We ONLY get the value from name here
  return name;
};

// 5. We also have default parameters now
const whoseFault = (blame = `your`) = {
  return `YOU KNOW IT'S ALL ${blame} fault`
};

// 6. Objects with key matches an already declared variable can simply just use a name
const thing = 1;

const obj = {
  thing
};

// 7. Finally, we can import and export modules in our code (browser coming soon).
// thing.js
const exPort = () => {
  return `We exported something`;
};

export default exPort;

// main.js
import exPort from './thing.js'; // Currently in Node es6 strings don't work on imports

export(); // We exported something;
```

Now pick up your jaw and settle down (I know right, is this even javascript???), take this in and start using these new features.

*It should go without saying that I didnt' capture all the changes in ES6 so please check the [official spec](http://www.ecma-international.org/ecma-262/6.0/) if your interested*

## React
Next let's look at React and why imo it's so cool. React IS NOT a framework, it's just a (really efficient) way to render the views in your app. What essentially happens is that React creates a representation of the DOM in memory. This means your your app doesn't try to update the entire DOM everytime some data changes, instead the virtual/value DOM is recreated and when it differs from the actual only the changed DOM nodes are updated, saving you time and headaches. I won't go into how the vdom works (b/c I have no clue) and that's outside the scope of this, but there's one more apsect to react that also helps writing views become easier: JSX.

JSX is a pseudo-representation of (J)ava(S)cript in an (X)ML like syntax, let's look at it in light detail:

``` javascript
import React from 'react';
import {render} from 'react-dom';

class Example extends React.Component {
  render () {
    return (
      <div>
        <h3> {`HELLO REACT`} </h3>
      </div>
    );
  }
};

render(<Example>, document.body);
```

Yup folks, it happened Javascript had it's first one night stand and it chose to get down and dirty with HTML. The best thing about React is that is let's you declaratively create view components and render them into your html. This example is simple, but trust me when I say that you can do alot with React.

Before you start running around like a developer with their Macbook fried and throwing a class in every component, let's be alittle more "functional" and compose components:

``` javascript
import React from 'react';
import {render} from 'react-dom';

const Hello = ({content}) => { // We destructure here, there's really an object, props, that holds passed variables but it's cleaner to use this syntax.
    <h3>{content}</h3>
};

const Example = ({msg}) => {
  <div>
    <Hello content={msg} />
  </div>
};

render(<Example msg=`Hello World`>, document.body);
```

So now we've removed our classes from our code, which prevents headaches, and turned them into *stateless* functions. Stateless means that our components, Example and Hello are simple composed, think f(g(x))) with x being our message g being Hello and f being Example. What's more these two components are totally independent of one another and can therefore be use somewhere else in our app without having to worry about inheritence or super classes or and other strings attached. *Spoiler Alert* this leads to much easier testing of the app as now the app is just a composition of smaller separate functional components. Another idea is that most of your components will be ***dumb*** (Hello) and just render some html based on what's passed in, but there are ***smart*** components (Example) that handle how the data is passed, how events are handled, etc... This will be explored at another time.

## Testing
Now testing is terrible, not because it actually is but more so due to how we think of testing. Riddle me this, if your code is super simple and functional:

``` javascript
const add = (x, y) => {
  return x + y;
};
```

Then why in the $%&* do you need to import hella dependencies (with their globals), test runners, harnesses and reporters as if we're doing some extravagant daredevil stunt:

``` javascript
import harness from 'harness-provider';

import add from './add.js';

beforeEachTest = () => {
  // Provide each test with some shared state
};

afterEactTest = () => {
  // Remove shared state
}

desribe(`Let's test`, () => {
  assert.equals(add(1,1), 2, 'Hey it added correctly');
};
```

Now this is a quite a mess. First, I dare you and Waldo to figure out where beforeEactTest, afterEactTest, describe and assert came from. Let me save you the time (I tried), they were added as global variables through some module somehow. So if you didn't have trust issues before, download some Drake, some red wine and get ready because the moment any of these break your whole world will shatter. Second, EachTest functions provide every single test with the same starting data which doesn't really mirror actual use and further more why introduce state in tests when we just worked so hard to keep *what* we were testing stateless? Instead a small assertion library own it's on should be used to test your modules, because you did try and use dumb and smart components right? Let's take a look at a nice solution for this from a module called tape:

``` javascript
import test from 'tape';

import add from './add.js';

test(`Let's test with tape!`, (swear) => {
  const
    swearJar = 1,
    bonafideResult = 2;

    swear.plan(swearJar);
    swear.assertEquals(add(1,1), 2, `Hey, our add worked!`);
});
```

You may think that this is about the same but try and find where test and swear come from...that's right test is straight from the tape module and swear is a callback function parameter that is LOCALLY SCOPPED TO THIS SINGLE TEST. See the benefit? This means there's no need for some magically before and after function, we can plan what we need within eact test and then it is destroyed once it runs. Additionally, we can plan ahead for how many assertions we want to perform (I find this helps).

*Please note I like to use swear and bonafide as my variable names but feel free to use any name you want as long as it makes sense to you.*

---

More to be added.

