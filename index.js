'use strict';

const
  express = require(`express`),

  app = express(),
  env = process.argv[2] || `prod`,
  port = env === `prod` ? 3000 : 2368;

app.use(express.static(`${__dirname}/app/dist`));

app.get(`*`, (req, res) => {
  res.sendFile(`${__dirname}/app/dist/index.html`);
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
