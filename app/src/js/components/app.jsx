import React, {Component, PropTypes} from 'react';

import {pacomoDecorator} from '../utils/pacomo';
import Input from './input.jsx';
import Output from './output.jsx';

@pacomoDecorator
export default class App extends Component {
  displayName = `App`;
  static propTypes = {
    placeHolderValue: PropTypes.string.isRequired
  };
  static defaultProps = {
    placeHolderValue: ``
  };
  state = {
    outputValue: ``
  };

  handleChange = (synEvent) => {
    this.setState({
      outputValue: synEvent.target.value
    });
  };


  render = () => {
    return (
      <div>
        <Input
            onChangeHandler={this.handleChange}
            placeHolder={this.props.placeHolderValue}
        />
        <Output value={this.state.outputValue} />
      </div>
    );
  }
}
