import React, {PropTypes} from 'react';

import {pacomoTransformer} from '../utils/pacomo';

const Output = ({value}) => {
  return (
    <div>
      <h3 className={`content`}>{value}</h3>
    </div>
  );
};

Output.displayName = `Output`;

Output.propTypes = {
  value: PropTypes.string.isRequired
};

export default pacomoTransformer(Output);
