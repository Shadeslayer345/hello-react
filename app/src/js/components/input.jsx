import React, {PropTypes} from 'react';

import {pacomoTransformer} from '../utils/pacomo';

const Input = ({placeHolder, onChangeHandler}) => {
  return (
    <div>
      <textarea
          className={`content`}
          onChange={onChangeHandler}
          placeholder={placeHolder}
      />
    </div>
  );
};

Input.displayName = `Input`;

Input.propTypes = {
  placeHolder: PropTypes.string.isRequired,
  onChangeHandler: PropTypes.func.isRequired
};

export default pacomoTransformer(Input);
