import React from 'react';
import {render} from 'react-dom';

import App from './components/app.jsx';

render(
  <App placeHolderValue={`Hello Solo`} />,
  document.getElementById(`horejserv-App-root`)
);
